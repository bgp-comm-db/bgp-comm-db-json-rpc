"""
"""

from werkzeug.wrappers import Request, Response
from jsonrpc import JSONRPCResponseManager
from jsonrpc.dispatcher import Dispatcher
from bgp_comm_db.db import *
from bgp_comm_db.environment import *


class DBConnection(object):

    """docstring for DBConnection"""

    def __init__(self):
        super(DBConnection, self).__init__()

    def __enter__(self):
        # connect to the database
        self.connection = engine.connect()

        # begin a non-ORM transaction
        self.trans = self.connection.begin()

        # bind an individual Session to the connection
        self.session = Session(bind=self.connection)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.commit()

        # rollback - everything that happened with the
        # Session above (including calls to commit())
        # is rolled back.
        self.session.close()
        self.trans.rollback()
        self.connection.close()  # return connection to the Engine

    def get_std_comm_json(self, community_str: str):
        std_comm = StdComm.get_std_comm(self.session, StdValue(community_str))

        std_comm_schema = std_comm.__class__.Schema()
        result = std_comm_schema.dumps(std_comm)
        return result.data


class BgpCommDB(object):

    """docstring for BgpCommDB"""

    dispatcher = Dispatcher()

    def __init__(self):
        super(BgpCommDB, self).__init__()

    @staticmethod
    @dispatcher.add_method
    def echo(string):
        return string

    @staticmethod
    @dispatcher.add_method
    def get_std_comm(community_str):
        with DBConnection() as db_connection:
            json = db_connection.get_std_comm_json(community_str)
        return json

    @Request.application
    def application(self, request):

        response = JSONRPCResponseManager.handle(
            request.get_data(cache=False, as_text=True), BgpCommDB.dispatcher)
        return Response(response.json, mimetype='application/json')

    def __call__(self, environ, start_response):
        return self.application(environ, start_response)
