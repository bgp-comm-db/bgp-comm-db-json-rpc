"""
"""

if __name__ == "__main__" and __package__ is None:
    __package__ = "bgp_comm_db_json_rpc"  # PEP 366


from werkzeug.serving import run_simple
from bgp_comm_db_json_rpc.bgp_comm_db import BgpCommDB

def make_app():
    return BgpCommDB()

def main():
    print("Running Server on http://localhost:4000 ...")
    run_simple('localhost', 4000, make_app())

if __name__ == '__main__':
    main()
