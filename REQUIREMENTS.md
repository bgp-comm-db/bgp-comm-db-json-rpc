Requirements
============

 + Web server with WSGI support:
     * apache2 with `mod_wsgi`
 + python3 (>= 3.3, tested 3.4)
 + bgp-comm-db
 + werkzeug (tested 0.10.4)
 + requests (tested 2.2.1)
 + json-rpc (tested 1.9.1)

A nice library for client usage is:
 + jsonrpclib-pelix
