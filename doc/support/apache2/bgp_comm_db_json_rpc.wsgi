#!/usr/bin/env python3

from bgp_comm_db_json_rpc.__main__ import make_app

application = make_app()
